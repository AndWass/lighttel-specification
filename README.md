# Lighttel specification

This repository contains the specification of the light telemetry (lighttel) protocol. It is currently very much work in progress but it aims to be a simple protocol to implement while still supporting advanced use-cases such as field gateway connection multiplexing and data compression.

In parallell to the specification a server is also being developed in Elixir. The server can be found [here](https://gitlab.com/Shakti213/lighttel-server). A client will also be made available at a later date.