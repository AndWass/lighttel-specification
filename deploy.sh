#!/bin/bash

mkdir .public
FILES=`ls *.part.html | sort -n -t _ -k 1`
touch .public/index.html
for f in $FILES
do
    cat $f >> .public/index.html
done
rm -r public
mv .public public